# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from sales.sample.testing import SALES_SAMPLE_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that sales.sample is properly installed."""

    layer = SALES_SAMPLE_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if sales.sample is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'sales.sample'))

    def test_browserlayer(self):
        """Test that ISalesSampleLayer is registered."""
        from sales.sample.interfaces import (
            ISalesSampleLayer)
        from plone.browserlayer import utils
        self.assertIn(
            ISalesSampleLayer,
            utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = SALES_SAMPLE_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['sales.sample'])

    def test_product_uninstalled(self):
        """Test if sales.sample is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'sales.sample'))

    def test_browserlayer_removed(self):
        """Test that ISalesSampleLayer is removed."""
        from sales.sample.interfaces import \
            ISalesSampleLayer
        from plone.browserlayer import utils
        self.assertNotIn(
           ISalesSampleLayer,
           utils.registered_layers())
